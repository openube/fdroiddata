Categories:Games
License:GPLv3
Web Site:http://learnmusicnotes.sourceforge.net
Source Code:http://sourceforge.net/projects/learnmusicnotes/develop
Issue Tracker:http://sourceforge.net/tracker/?group_id=371992

Auto Name:Learn Music Notes
Summary:Music sight reading training game
Description:
A simple game to assist with music sight reading training.
.

Repo Type:git-svn
Repo:https://learnmusicnotes.svn.sourceforge.net/svnroot/learnmusicnotes/trunk

Build Version:1.2,3,16
Build Version:1.4,5,20,forceversion=yes,forcevercode=yes

Auto Update Mode:None
Update Check Mode:Static
Current Version:1.4
Current Version Code:5

