Categories:Multimedia
License:LGPLv3
Web Site:http://www.videolan.org/vlc/download-android.html
Source Code:http://git.videolan.org/?p=vlc-ports/android.git;a=summary
Issue Tracker:http://www.videolan.org/support/index.html#bugs
Donate:http://www.videolan.org/contribute.html#money

Auto Name:VLC
Summary:Media player
Description:
Video and audio player that supports a wide range of formats,
for both local and remote playback.

[http://git.videolan.org/?p=vlc-ports/android.git;a=blob_plain;f=NEWS NEWS]
.

#Instructions and dependencies here: http://wiki.videolan.org/AndroidCompile
#see http://buildbot.videolan.org/builders/ for version code scheme
Repo Type:git
Repo:git://git.videolan.org/vlc-ports/android.git

Build:0.0.11-ARMv7,110
    commit=0.0.11
    subdir=vlc-android
    update=.,../java-libs/SlidingMenu,../java-libs/ActionBarSherlock
    forceversion=yes
    forcevercode=yes
    prebuild=sed -i '48d' ../Makefile
    build=cd ../ && \
        ANDROID_ABI=armeabi-v7a ANDROID_SDK=$$SDK$$ ANDROID_NDK=$$NDK$$ ./compile.sh release
    buildjni=no

Build:0.0.11-ARM,111
    commit=0.0.11
    subdir=vlc-android
    update=.,../java-libs/SlidingMenu,../java-libs/ActionBarSherlock
    forceversion=yes
    forcevercode=yes
    prebuild=sed -i '48d' ../Makefile
    build=cd ../ && \
        ANDROID_ABI=armeabi ANDROID_SDK=$$SDK$$ ANDROID_NDK=$$NDK$$ ./compile.sh release
    buildjni=no

Build:0.0.11-x86,112
    disable=ffmpeg error 0.0.11
    commit=unknown - see disabled
    subdir=vlc-android
    update=.,../java-libs/SlidingMenu,../java-libs/ActionBarSherlock
    forceversion=yes
    forcevercode=yes
    prebuild=sed -i '48d' ../Makefile
    build=cd ../ && \
        ANDROID_ABI=x86 ANDROID_SDK=$$SDK$$ ANDROID_NDK=$$NDK$$ ./compile.sh release
    buildjni=no

Build:0.0.11-mips,113
    commit=0.0.11
    subdir=vlc-android
    update=.,../java-libs/SlidingMenu,../java-libs/ActionBarSherlock
    forceversion=yes
    forcevercode=yes
    prebuild=sed -i '48d' ../Makefile
    build=cd ../ && \
        ANDROID_ABI=mips ANDROID_SDK=$$SDK$$ ANDROID_NDK=$$NDK$$ ./compile.sh release
    buildjni=no

Build:0.1.3-MIPS,1301
    disable=build failing (at 0.1.3)
    commit=0.1.3
    subdir=vlc-android
    forceversion=yes
    forcevercode=yes
    srclibs=VLC@7c52aacbe
    patch=ndkr9.patch
    prebuild=sed -i '/ant/d' ../Makefile && \
        ln -s vlc-android/$$VLC$$ ../vlc
    build=cd ../ && \
        ANDROID_ABI=mips ANDROID_SDK=$$SDK$$ ANDROID_NDK=$$NDK$$ ./compile.sh release
    buildjni=no

Build:0.1.3-x86,1302
    commit=0.1.3
    subdir=vlc-android
    forceversion=yes
    forcevercode=yes
    srclibs=VLC@7c52aacbe
    patch=ndkr9.patch
    prebuild=sed -i '/ant/d' ../Makefile && \
        ln -s vlc-android/$$VLC$$ ../vlc
    build=cd ../ && \
        ANDROID_ABI=x86 ANDROID_SDK=$$SDK$$ ANDROID_NDK=$$NDK$$ ./compile.sh release
    buildjni=no

Build:0.1.3-ARM,1303
    commit=0.1.3
    subdir=vlc-android
    forceversion=yes
    forcevercode=yes
    srclibs=VLC@7c52aacbe
    patch=ndkr9.patch
    prebuild=sed -i '/ant/d' ../Makefile && \
        ln -s vlc-android/$$VLC$$ ../vlc
    build=cd ../ && \
        ANDROID_ABI=armeabi ANDROID_SDK=$$SDK$$ ANDROID_NDK=$$NDK$$ ./compile.sh release
    buildjni=no

Build:0.1.3-ARMv7,1304
    commit=0.1.3
    subdir=vlc-android
    forceversion=yes
    forcevercode=yes
    srclibs=VLC@7c52aacbe
    patch=ndkr9.patch
    prebuild=sed -i '/ant/d' ../Makefile && \
        ln -s vlc-android/$$VLC$$ ../vlc
    build=cd ../ && \
        ANDROID_ABI=armeabi-v7a ANDROID_SDK=$$SDK$$ ANDROID_NDK=$$NDK$$ ./compile.sh release
    buildjni=no

Build:0.9.0,9002
    commit=0.9.0
    subdir=vlc-android
    forcevercode=yes
    srclibs=VLC@31ffb20309264994
    prebuild=sed -i '/ant/d' ../Makefile && \
        ln -s vlc-android/$$VLC$$ ../vlc
    build=cd ../ && \
        ANDROID_ABI=x86 ANDROID_SDK=$$SDK$$ ANDROID_NDK=$$NDK$$ ./compile.sh release
    buildjni=no

Build:0.9.0,9004
    commit=0.9.0
    subdir=vlc-android
    forcevercode=yes
    srclibs=VLC@31ffb20309264994
    prebuild=sed -i '/ant/d' ../Makefile && \
        ln -s vlc-android/$$VLC$$ ../vlc
    build=cd ../ && \
        ANDROID_ABI=armeabi-v7a ANDROID_SDK=$$SDK$$ ANDROID_NDK=$$NDK$$ ./compile.sh release
    buildjni=no

Build:0.9.1,9102
    commit=0.9.1
    subdir=vlc-android
    forcevercode=yes
    srclibs=VLC@37e886d113b8b567c15208579fb2f
    prebuild=sed -i '/ant/d' ../Makefile && \
        ln -s vlc-android/$$VLC$$ ../vlc
    build=cd ../ && \
        ANDROID_ABI=x86 ANDROID_SDK=$$SDK$$ ANDROID_NDK=$$NDK$$ ./compile.sh release
    buildjni=no

Build:0.9.1,9104
    commit=0.9.1
    subdir=vlc-android
    forcevercode=yes
    srclibs=VLC@37e886d113b8b567c15208579fb2f
    prebuild=sed -i '/ant/d' ../Makefile && \
        ln -s vlc-android/$$VLC$$ ../vlc
    build=cd ../ && \
        ANDROID_ABI=armeabi-v7a ANDROID_SDK=$$SDK$$ ANDROID_NDK=$$NDK$$ ./compile.sh release
    buildjni=no

# +0: - (upstream)
# +1: mips
# +2: x86
# +3: arm
# +4: armv7 (CV)
Archive Policy:4 versions
Auto Update Mode:None
Update Check Mode:Tags
Vercode Operation:%c + 4
Current Version:0.9.1
Current Version Code:9104

