AntiFeatures:NonFreeNet
Categories:Office
License:GPLv2
Web Site:http://namesorts.com
Source Code:https://github.com/namsor/gendreapp
Issue Tracker:https://github.com/namsor/gendreapp/issues

Auto Name:Gendre
Summary:Predict gender from contact name
Description:
Query an online service on the estimated gendre of your contacts based
on their name.
.

Repo Type:git
Repo:https://github.com/namsor/gendreapp.git

Build:0.0.4,2
    commit=v0.0.4
    rm=dist
    extlibs=android/android-support-v4.jar
    srclibs=1:Support/v7/appcompat@android-4.4_r1.2
    prebuild=cp libs/android-support-v4.jar $$Support$$/libs

Build:0.0.5,3
    commit=v0.0.5
    rm=dist
    extlibs=android/android-support-v4.jar
    srclibs=1:Support/v7/appcompat@android-4.4_r1.2
    prebuild=cp libs/android-support-v4.jar $$Support$$/libs

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.0.5
Current Version Code:3

