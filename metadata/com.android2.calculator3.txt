Categories:Office
License:Apache2
Web Site:https://github.com/Xlythe/android_packages_apps_Calculator
Source Code:https://github.com/Xlythe/android_packages_apps_Calculator
Issue Tracker:https://github.com/Xlythe/android_packages_apps_Calculator/issues
Donate:https://www.gittip.com/Xlythe

Auto Name:Calculator
Summary:Make calculations
Description:
A simple calculator that tries to make graphing and matrix math as simple as adding or multiplying
two numbers. Slide left and right to access more panels.
.

Repo Type:git
Repo:https://github.com/Xlythe/android_packages_apps_Calculator.git

Build Version:1.8,22,d2b2bde6c9,target=android-15
Build Version:2.0,24,d61631afc5,target=android-16,srclibs=Slider@556b4db69a,prebuild=\
rm -rf tests libs/slider.jar && echo "android.library.reference.1=$$Slider$$" >> project.properties
Build Version:2.2,26,!no source
Build Version:2.2.2,28,2eafd952,target=android-16,srclibs=Slider@556b4db69a,prebuild=\
rm -rf tests libs/slider.jar && echo "android.library.reference.1=$$Slider$$" >> project.properties
Build Version:3.1.2,42,b427694d,target=android-17,srclibs=Slider@4d2c894ca3,prebuild=\
rm -rf tests libs/slider.jar && echo "android.library.reference.1=$$Slider$$" >> project.properties
Build Version:v3.2,43,ff027e7311a69aea68,target=android-17,srclibs=Slider@4d2c894ca3,prebuild=\
rm -rf tests libs/slider.jar && echo "android.library.reference.1=$$Slider$$" >> project.properties
Build Version:v3.3.2,46,02a0d7cccf1182d4,target=android-17,srclibs=Slider@4d2c894ca3,prebuild=\
rm -rf tests libs/slider.jar && echo "android.library.reference.1=$$Slider$$" >> project.properties
Build Version:v3.4,47,4df0d69e720fd2ae2,target=android-17,srclibs=Slider@2ec23c12a39,prebuild=\
rm -rf tests libs/slider.jar && echo "android.library.reference.1=$$Slider$$" >> project.properties
Build Version:v3.4.2,49,799803d7f0,target=android-17,srclibs=Slider@2ec23c12a39,prebuild=\
rm -rf tests libs/slider.jar && echo "android.library.reference.1=$$Slider$$" >> project.properties

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:v3.4.2
Current Version Code:49

