AntiFeatures:UpstreamNonFree
Categories:Games
License:NCSA
Web Site:https://xlythe.github.com/HexGame_Android
Source Code:https://github.com/Xlythe/HexGame_Android
Issue Tracker:https://github.com/Xlythe/HexGame_Android/issues

Auto Name:Hex
Summary:Two-player game
Description:
Hex is a simple board game where two players race to connect their sides of
the board.
The winner is the first to build a solid path connecting their two sides.

Newer versions of this app use the non-free Google Play Services library.
.

Repo Type:git
Repo:https://github.com/Xlythe/HexGame_Android.git

Build:2.2,13
    commit=900717
    target=android-15

Build:2.3,14
    commit=ed1053230e77e13a
    target=android-15

Build:3.0.2,22
    commit=2010ac7d1a1ab
    disable=Uses GMS
    target=android-17
    srclibs=1:ActionBarSherlock@4.4.0

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:3.0.2
Current Version Code:22

