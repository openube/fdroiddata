Categories:Security,Internet
License:NewBSD
Web Site:http://www.torproject.org/docs/android.html.en
Source Code:https://gitweb.torproject.org/orbot.git
Issue Tracker:https://dev.guardianproject.info/projects/orbot/issues
Donate:https://www.torproject.org/donate/donate.html.en
FlattrID:5649

Auto Name:Orbot
Summary:Tor (anonymity) client
Description:
Tor is both software and an open network that helps you defend against network
surveillance that
threatens personal freedom and privacy, confidential business activities and
relationships.

Orbot allows access to Tor by accessing a local SOCKS or HTTP proxy.
On a rooted device, the proxying can be completely transparent i.e. the app
that accesses the network need not be aware of the proxy's existence;
you can choose which apps go via the proxy in the settings.

If you don't have root access, there are some apps that are designed to work
closely with tor or allow proxied connections:
[[info.guardianproject.otr.app.im]], [[info.guardianproject.browser]] and
[[org.mariotaku.twidere]].
There is also a proxy configurator addon for [[org.mozilla.firefox]] called
[https://github.com/guardianproject/ProxyMob/downloads ProxyMob] (not yet
available from the Mozilla addon site).

Requires root: No, but you will need to use apps that allow proxies if root
is not granted.
.

Repo Type:git
Repo:https://git.torproject.org/orbot.git

Build:0.2.3.23-rc-1.0.11-RC5-test2,51
    commit=ff38bf4e5
    submodules=yes
    srclibs=ActionBarSherlock@4.1.0
    prebuild=sed -i 's@\(android.library.reference.1=\).*@\1$$ActionBarSherlock$$@' project.properties && \
        export NDK_BASE=$$NDK$$ && \
        export PATH=$PATH:$$NDK$$/toolchain/bin && \
        sed -i 's/ndk-build/\$(NDK_BASE)\/ndk-build/g' external/Makefile && \
        make -C external

Build:13.0.5,70
    commit=13.0.5
    submodules=yes
    rm=libs
    patch=buildfixes.patch
    build=NDK_BASE=$$NDK$$ make -C external

Build:13.0.6-RC-1,79
    commit=13.0.6-RC-1
    submodules=yes
    rm=libs,external/superuser-commands/RootCommands-Demo/libs
    prebuild=echo 'target=android-17' >> external/superuser-commands/RootCommands-Library/project.properties && \
        mkdir libs
    build=NDK_BASE=$$NDK$$ make -C external

Auto Update Mode:None
Update Check Mode:Tags
Current Version:13.0.6-RC-1
Current Version Code:79

