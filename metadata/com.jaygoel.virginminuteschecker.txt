AntiFeatures:UpstreamNonFree
Categories:Office
License:NewBSD
Web Site:http://www.jaygoel.com
Source Code:https://github.com/poundifdef/VirginMobileMinutesChecker
Issue Tracker:https://github.com/poundifdef/VirginMobileMinutesChecker/issues

Auto Name:Minutes Checker for Virgin Mobile
Summary:For Virgin Mobile US customers
Description:
Fetches info about the Beyond Talk plan
from Virgin Mobile's servers but isn't officially endorsed
by the company.

A proprietary ad library was removed.
.

Repo Type:git
Repo:https://github.com/poundifdef/VirginMobileMinutesChecker.git

Build Version:1.12,13,dc1d1c020c05f,\
srclibs=1:MobAdMob@2d5736,\
subdir=VirginMobileMinutesChecker,\
rm=VirginMobileMinutesChecker/libs/GoogleAdMobAdsSdk-4.1.1.jar

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.12
Current Version Code:13


