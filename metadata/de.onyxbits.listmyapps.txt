Categories:System
License:Apache2
Web Site:https://github.com/onyxbits/listmyaps
Source Code:https://github.com/onyxbits/listmyaps
Issue Tracker:https://github.com/onyxbits/listmyaps/issues

Auto Name:List My Apps
Summary:List apps
Description:
Compiles a list of apps installed on the device, in a text format for easy
sharing.
.

Repo Type:git
Repo:https://github.com/onyxbits/listmyaps.git

Build:1.2,3
    commit=v1.2
    target=android-17
    rm=custom_rules.xml

Build:1.3,4
    commit=v1.3
    rm=custom_rules.xml

Build:2.0,5
    commit=v2.0
    rm=custom_rules.xml
    extlibs=android/android-support-v4.jar

Build:2.1,6
    commit=v2.1
    rm=custom_rules.xml
    extlibs=android/android-support-v4.jar

Build:2.2,7
    commit=v2.2
    rm=custom_rules.xml
    extlibs=android/android-support-v4.jar

Build:2.3,8
    commit=v2.3
    rm=custom_rules.xml
    extlibs=android/android-support-v4.jar

Build:2.4,9
    commit=v2.4
    rm=custom_rules.xml
    extlibs=android/android-support-v4.jar

Build:3.0,10
    commit=v3.0
    rm=custom_rules.xml
    extlibs=android/android-support-v4.jar

Build:3.1,11
    commit=v3.1
    rm=custom_rules.xml
    extlibs=android/android-support-v4.jar

Build:3.2,12
    commit=v3.2
    rm=custom_rules.xml
    extlibs=android/android-support-v4.jar

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:3.2
Current Version Code:12

