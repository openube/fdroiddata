Categories:Navigation
License:Apache2
Web Site:https://www.traccar.org
Source Code:https://github.com/tananaev/traccar-client-android
Issue Tracker:https://github.com/tananaev/traccar-client-android/issues
Donate:http://www.traccar.org/donate.jsp

Auto Name:Traccar Client
Summary:Track your location
Description:
Client for Traccar server software. A demo server is setup in the app
for trying it out and the results can be viewed in a browser.
.

Repo Type:git
Repo:https://github.com/tananaev/traccar-client-android.git

Build:2.7,14
    commit=10745c

Build:2.9,16
    commit=39e126cc6d91
    subdir=traccar-client
    gradle=yes

Build:2.10,17
    commit=ea89c638b79d0cf003a684a1bd61
    subdir=traccar-client
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:2.11
Current Version Code:18

