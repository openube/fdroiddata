Categories:System
License:GPLv3
Web Site:http://sites.google.com/site/ghostcommander1
Source Code:http://sourceforge.net/p/gc-samba/code
Issue Tracker:http://sourceforge.net/p/gc-samba/_list/tickets
Donate:http://sourceforge.net/p/ghostcommander/donate

Auto Name:Ghost Commander - Samba plugin
Summary:Access files on the network
Description:
A plug-in for [[com.ghostsq.commander]] to access to Windows network shared
folders (also referenced as SMB, Samba, CIFS or even NetBios).
Launch Ghost Commander and go to 'Menu > Location > Home > Windows share'.
Alternatively, scroll along the toolbar until you arrive at 'Home'.
Enter your server name and credentials (if any), then tap the 'Connect' button.
Once connected, you should be able to see a list of Windows machines or
Samba servers on the network. Tip: Better have the Windows account with
password. A passwordless Windows account might not work.
.

# If you are building this yourself, config.py must be edited to sign this
# with the same key as Ghost Commander
Repo Type:git-svn
Repo:https://svn.code.sf.net/p/gc-samba/code

Build:1.21.3b1,47
    disable=dexclassloader crash: see forum (at 102)
    commit=102
    init=rm -rf gen/ && \
        svn co -r407 https://ghostcommander.svn.sourceforge.net/svnroot/ghostcommander gc
    extlibs=jcifs/jcifs-1.3.17.jar
    prebuild=sed -i 's/Utils.getCause( e )/e.getMessage()/' src/com/ghostsq/commander/samba/SMBAdapter.java
    build=ant release -f gc/build.xml && \
        cd libs/ && \
        cp -a ../gc/bin/classes/com . && \
        zip -r -9 gc.jar com/ && \
        rm -rf com/

Build:1.21.3,50
    disable=Build problems
    commit=unknown - see disabled

Build:1.30,51
    commit=106
    extlibs=jcifs/jcifs-1.3.17.jar;custom_rules.xml
    srclibs=GhostCommander@390
    build=mv libs/custom_rules.xml . && \
        ant debug -f $$GhostCommander$$/build.xml && \
        jar c -C $$GhostCommander$$/bin/classes/ com > gc.jar && \
        install -D gc.jar libs/gc.jar

Build:1.31,52
    commit=108
    extlibs=jcifs/jcifs-1.3.17.jar;custom_rules.xml
    srclibs=GhostCommander@390
    build=mv libs/custom_rules.xml . && \
        ant debug -f $$GhostCommander$$/build.xml && \
        jar c -C $$GhostCommander$$/bin/classes/ com > gc.jar && \
        install -D gc.jar libs/gc.jar

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.31.1b3
Current Version Code:55

