Categories:System
License:NewBSD
Web Site:https://code.google.com/p/perapp
Source Code:https://code.google.com/p/perapp/source
Issue Tracker:https://code.google.com/p/perapp/issues
Donate:http://forum.xda-developers.com/donatetome.php?u=2714177

Auto Name:PerApp
Summary:Separate settings for each app
Description:
Easily extendable per-app settings app for Android. Orientation lock, screen timeout , volume and
more can be adjusted.
.

Repo Type:git-svn
Repo:https://perapp.googlecode.com/svn/trunk

Build Version:0.12,14,22
Build Version:1.02,19,30
Build:1.03,20
	commit=31
	disable=missing layouts and views

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.03
Current Version Code:20

