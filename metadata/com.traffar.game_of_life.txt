#Categories:Games,Mathematics
Categories:Games,Science & Education
License:GPLv3
Web Site:https://bitbucket.org/przemekr/game_of_life_agg
Source Code:https://bitbucket.org/przemekr/game_of_life_agg/src
Issue Tracker:https://bitbucket.org/przemekr/game_of_life_agg/issues

Auto Name:Game of Life
Summary:Conway's Game of Life simulation
Description:
Implementation of [http://en.wikipedia.org/wiki/Conway%27s_Game_of_Life Conway's Game of Life]
with a nice user interface.

Features include:
* Easy to use, intuitive interface
* Number of initial states to try out
* Resize the universe and set the evolution speed
.

Repo Type:git
Repo:https://bitbucket.org/przemekr/game_of_life_agg.git

Build:20131217,1
    commit=android20131217
    subdir=android
    init=cd jni/SDL && \
        rm -rf src include SDL-2.0.1* && \
        wget http://libsdl.org/release/SDL2-2.0.1.tar.gz && \
        tar xf SDL2-2.0.1.tar.gz && \
        ln -s SDL2-2.0.1/src . && \
        ln -s SDL2-2.0.1/include .
    buildjni=yes

Build:0.2,2
    commit=0.2
    subdir=android
    init=cd jni/SDL && \
        rm -rf src include SDL-2.0.1* && \
        wget http://libsdl.org/release/SDL2-2.0.1.tar.gz && \
        tar xf SDL2-2.0.1.tar.gz && \
        ln -s SDL2-2.0.1/src . && \
        ln -s SDL2-2.0.1/include .
    buildjni=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:0.2
Current Version Code:2
