Categories:Phone & SMS
License:GPLv3
Web Site:https://code.google.com/p/quitesleep
Source Code:https://code.google.com/p/quitesleep/source
Issue Tracker:https://code.google.com/p/quitesleep/issues

Auto Name:QuiteSleep
Summary:Answer calls when you're busy
Description:
QuiteSleep is an application for Android 2.0+ which manages your
incoming calls when you're busy, sleeping, or doing anything between
two time slots in any day of the week. QuiteSleep can block a list
of callers or even other different incoming calls and send an SMS
or e-mail with a predefined text to notify you're busy, sleeping, etc.

Four different ways to allow who can call you:

1) block all; 2) block only blocked contacts; 3) block unknown 4) block unknown and blocked contacts
.

Repo Type:hg
Repo:https://code.google.com/p/quitesleep

Build Version:2.0,5,fbf8f2db2b36,prebuild=rm -rf libs && mv lib libs
Build Version:2.0.3,9,619ef22277cc,prebuild=rm -rf libs && mv lib libs
Build Version:3,13,0a3dcf9b9c25,subdir=quitesleep3,prebuild=rm -rf libs && mv lib libs,update=.;../com_actionbarsherlock;../com_viewpagerindicator
Build Version:3,14,!Different version code elsewhere,subdir=quitesleep3

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:3
Current Version Code:13

