Categories:Wallpaper
License:Apache2
Web Site:https://code.google.com/p/scrollablecontacts
Source Code:https://code.google.com/p/scrollablecontacts/source
Issue Tracker:https://code.google.com/p/scrollablecontacts/issues
Donate:https://code.google.com/p/scrollablecontacts

Auto Name:Contact Widget
Summary:Contact widget
Description:
Requires a homescreen that supports scrollable widgets.
.

Repo Type:git-svn
Repo:https://scrollablecontacts.googlecode.com/svn/trunk

Build Version:1.0.2,102,129,target=android-15

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0.2
Current Version Code:102

No Source Since:1.0.3

