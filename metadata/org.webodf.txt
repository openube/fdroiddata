Categories:Office
License:AGPL
Web Site:http://webodf.org
Source Code:https://github.com/kogmbh/WebODF
Issue Tracker:https://github.com/kogmbh/WebODF/issues

Auto Name:WebODF
Summary:ODF Document Viewer
Description:
WebODF is a JavaScript library which makes it easy to add Open
Document Format (ODF) support to your website and to your mobile
or desktop application. It uses HTML and CSS to display ODF
documents.

This app lets you view Open Document format files directly.
.

Repo Type:git
Repo:https://github.com/kogmbh/WebODF.git

Build Version:1.2,3,!broken app script needs fixing 5262fc,subdir=programs/android,submodules=no,update=no,\
init=cd ../../ && sed -i '4\,6d' .gitmodules && sed -i '12\,13d' .git/config && \
git rm --cached simplerevisionserver/pywebdav && git submodule init && git submodule update && \
cd programs/android,\
prebuild=android update project -p . -s -t android-10

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.2
Current Version Code:3

