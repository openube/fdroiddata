Categories:System
License:GPLv3
Web Site:http://jecelyin.com
Source Code:https://github.com/jecelyin/920-Text-Editor
Issue Tracker:https://github.com/jecelyin/920-Text-Editor/issues
Donate:http://www.jecelyin.com/donate.html

#if the app is going to be at the top of the list then get rid of the ugly title
Name:920 Editor
Auto Name:920 Text Editor
Summary:Text editor
Description:
View and edit code (even as root). Multi-tab, syntax-highlighting and more.

Status: The app is now using resources from AOSP so it's unlikely we'll
be able to update it.
.

Repo Type:git
#Repo:https://920-text-editor.googlecode.com/svn/trunk/ ; old repo
Repo:https://github.com/jecelyin/920-Text-Editor.git

#Build Version:12.4.25,32,17,buildjni=yes
Build Version:12.8.26,33,0c47ee4c17,buildjni=yes
Build Version:12.8.29,34,!No source code published,buildjni=yes
Build Version:12.9.25,35,!No source code published,buildjni=yes
Build Version:12.11.23,39,6ecd84ce9c,forceversion=yes,forcevercode=yes,buildjni=yes
Build Version:13.07.18,46,!uses AOSP libraries at 1a5f84489c5fd6,buildjni=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:13.7.18
Current Version Code:46

