Categories:Wallpaper
License:GPLv2
Web Site:https://github.com/PuZZleDucK/Orbital-Live-Wallpaper
Source Code:https://github.com/PuZZleDucK/Orbital-Live-Wallpaper
Issue Tracker:https://github.com/PuZZleDucK/Orbital-Live-Wallpaper/issues

Auto Name:OrbitalLiveWallpaper
Summary:Animated wallpaper
Description:
Rotating circles that respond to your touch. Wallpaper that takes up little ram.
.

Repo Type:git
Repo:https://github.com/PuZZleDucK/Orbital-Live-Wallpaper.git

Build Version:1.0,1,3cdb9d,subdir=Orbital-Live-Wallpaper
Build Version:1.2,3,855f619e,subdir=Orbital-Live-Wallpaper,target=android-8
Build Version:2.0,4,ea32533f,subdir=Orbital-Live-Wallpaper,target=android-8

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:2.0
Current Version Code:4

