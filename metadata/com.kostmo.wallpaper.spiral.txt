Categories:Wallpaper
License:Apache2
Web Site:https://code.google.com/p/android-spiral-wallpaper
Source Code:https://code.google.com/p/android-spiral-wallpaper/source
Issue Tracker:https://code.google.com/p/android-spiral-wallpaper/issues

Auto Name:HypnoTwister
Summary:An animated spiral live wallpaper
Description:
Draws animated mathematically perfect spirals on your home screen.
.

Repo Type:git-svn
Repo:https://android-spiral-wallpaper.googlecode.com/svn/trunk/app

Build Version:1.4,12,14,prebuild=mv lib libs
Build Version:1.4.1,13,16

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.4.1
Current Version Code:13

