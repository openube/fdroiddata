Categories:System
License:GPLv3
Web Site:http://www.tnhh.net/2013/04/02/sophia-ime-easter-awesomeness.html
Source Code:https://github.com/htruong/sophia_oss
Issue Tracker:https://github.com/htruong/sophia_oss/issues

Auto Name:Sophia keyboard
Summary:Android Latin keyboard
Description:
Keyboard based on AOSP Latin IME, built entirely with Android SDK/NDK.
For Vietnamese users, Sophia Vietnamese IME automatically adds tone-marks
to your Vietnamese input.
.

Repo Type:git
Repo:https://github.com/htruong/sophia_oss.git

Build:1.0.6,6
    commit=caab7
    subdir=latinime
    update=.;../inputmethodcommon;../support-v4
    prebuild=sed -i '11s/8/11/g' AndroidManifest.xml
    build=mkdir -p libs && \
        cd ../inputmethodcommon && \
        ant release && \
        cd .. && \
        cp inputmethodcommon/bin/classes.jar latinime/libs/inputmethodcommon.jar && \
        cd support-v4 && \
        ant release && \
        cd .. && \
        cp support-v4/bin/classes.jar latinime/libs/android-support-v4.jar
    buildjni=yes

Maintainer Notes:
It's possible Gingerbread support will appear in next version, so remove the
manifest patch
.

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0.10
Current Version Code:10

